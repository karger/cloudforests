# cloudforests

Codes for:
Limited protection and ongoing loss of tropical cloud forest
biodiversity and ecosystems worldwide
Dirk Nikolaus Karger1,2,3,*, Michael Kessler4, Marcus Lehnert5, Walter Jetz2,3
1 Swiss Federal Research Institute for Forest, Snow, and Landscape Research (WSL),
Zürcherstrasse 111, 8903 Birmensdorf, Switzerland
2 Department of Ecology and Evolutionary Biology, Yale University, 165 Prospect Street,
New Haven, CT 06520-8106, USA
3 Center for Biodiversity and Global Change, Yale University, 165 Prospect Street, New
Haven, CT 06520-8106, USA
4 Department of Systematic and Evolutionary Botany, University of Zurich, Zollikerstrasse
107, 8008 Zürich, Switzerland
5 Martin-Luther-University Halle-Wittenberg, Neuwerk 21, 06108 Halle (Saale), Germany
*e-mail: dirk.karger@wsl.ch
